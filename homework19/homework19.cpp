// homework19.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

class Animal {
public:
    Animal() : m_voice("?!") {
    }
    Animal(std::string voice) : m_voice(voice) {
    }
    virtual void Voice() {
        std::cout << "Animal: ";
        std::cout << m_voice << std::endl;
    }
    std::string GetVoice() {
        return m_voice;
    }
private:
    std::string m_voice;
};

class Dog : public Animal
{
public:
    Dog() : Animal("Gav-gav") {
    }
    void Voice() override {
        std::cout << "Dog: ";
        std::cout << GetVoice() << std::endl;
    }
private:

};

class Cat : public Animal
{
public:
    Cat() : Animal("Miau-miau") {
    }
    void Voice() override {
        std::cout << "Cat: ";
        std::cout << GetVoice() << std::endl;
    }
private:

};

class Fish : public Animal
{
public:
    Fish() : Animal("Bull-bull") {
    }
    void Voice() override {
        std::cout << "Fish: ";
        std::cout << GetVoice() << std::endl;
    }
private:

};

int main()
{
    Animal **animals = new Animal* [3];
    animals[0] = new Dog;
    animals[1] = new Cat;
    animals[2] = new Fish;
    for (int i = 0; i < 3; ++i)
        animals[i]->Voice();
}


