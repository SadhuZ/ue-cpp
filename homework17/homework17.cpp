// homework17.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

struct Coordinates {
	double x;
	double y;
	double z;
};

class Vector
{
public:
	Vector() : coordinate{ 0, 0, 0 }
	{}
	Vector(double x, double y, double z) : coordinate{ x, y, z }
	{}
	Vector(const Coordinates& coordinate) : coordinate{ coordinate.x, coordinate.y, coordinate.z }
	{}
	void PrintCoordinates() {
		std::cout << "(" << coordinate.x << ", " << coordinate.y  << ", " << coordinate.z << ")\n";
	}
	void SetCoordinates(double x, double y, double z) {
		coordinate.x = x;
		coordinate.y = y;
		coordinate.z = z;
	}
	void SetCoordinates(const Coordinates& coordinate) {
		this->coordinate.x = coordinate.x;
		this->coordinate.y = coordinate.y;
		this->coordinate.z = coordinate.z;
	}
	Coordinates GetCoordinates() {
		return coordinate;
	}
	double Length() {
		return sqrt(coordinate.x * coordinate.x + coordinate.y * coordinate.y + coordinate.z * coordinate.z);
	}
private:
	Coordinates coordinate;
};


int main()
{
	Vector v1;
	v1.SetCoordinates(1.1, 2.2, 3.3);
	v1.PrintCoordinates();
	Vector v2(1,2,3);
	v2.PrintCoordinates(); 
	Vector v3(v2.GetCoordinates());
	v3.PrintCoordinates();

	std::cout << "v3 length is " << v3.Length() << std::endl;
}