// homework16.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <ctime>

int main()
{
    const int size = 3;
    int arr[size][size];
    for (int i = 0; i < size; ++i)
        for (int j = 0; j < size; ++j)
            arr[i][j] = i + j;
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j)
            std::cout << arr[i][j] << " ";
        std::cout << std::endl;
    }

    time_t sec = time(NULL);
    tm timeInfo;
    localtime_s(&timeInfo, &sec);
    int row = timeInfo.tm_mday % size;
    int sum = 0;
    for (int i = 0; i < size; ++i) {
        if (i == row) {
            for (int j = 0; j < size; ++j) {
                sum += arr[i][j];
            }
        }
    }
    std::cout << "Row: " << row << ", sum = " << sum << std::endl;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
