#pragma once

template<class T> class Stack
{
private:
	T* stack;
	int size;
	int memorySize;
	void MemRealloc();
public:
	Stack();
	~Stack();
	int Size();
	void Push(T);
	T Pop();
	bool IsEmpty();
}; 

template<class T> Stack<T>::Stack() {
	size = 0;
	memorySize = 1;
	stack = new T(memorySize);
}

template<class T> Stack<T>::~Stack() {
	delete []stack;
}

template<class T> int Stack<T>::Size() {
	return size * sizeof(T);
}

template<class T> bool Stack<T>::IsEmpty() {
	return size == 0;
}

template<class T> void Stack<T>::MemRealloc() {
	if (size == memorySize) {
		memorySize *= 2;
		T* arr = new T[memorySize];
		for (size_t i = 0; i < size; ++i)
			arr[i] = stack[i];
		delete[] stack;
		stack = arr;
	}
}

template<class T> void Stack<T>::Push(T item) {
	MemRealloc();
	stack[size] = item;
	size++;
	//std::cout << size << " " << memorySize << std::endl;
}

template<class T> T Stack<T>::Pop(){
	T item = 0;
	if (size > 0) {
		item = stack[size - 1];
		size--;
	}
	return item;
}