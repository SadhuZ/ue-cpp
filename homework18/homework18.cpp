// homework18.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "Stack.h"

int main()
{
    Stack<int>* s = new Stack<int>;
    for (int i = 1; i < 10; ++i)
        s->Push(i);
    std::cout << "Size = " << s->Size() << std::endl;
    while(!s->IsEmpty())
        std::cout << s->Pop() << std::endl;

    delete s;
}