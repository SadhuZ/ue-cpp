// homework14.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

int main()
{
    std::string str1 = "string 1";
    std::string str2("string 2 longer than string 1");
    std::string str3{"string 3 longer than string 1 and string 2"};
    std::cout << str1 << ". Length: " << str1.length() 
        << ". First character is " << str1[0] 
        << ". Last character is " << str1[str1.length() - 1] << "." << std::endl;
    std::cout << str2 << ". Length: " << str2.size()
        << ". First character is " << str2.at(0)
        << ". Last character is " << str2.at(str2.size() - 1) << "." << std::endl;
    std::cout << str3 << ". Length: " << str3.end() - str3.begin()
        << ". First character is " << *str3.begin()
        << ". Last character is " << *(str3.end() - 1) << "." << std::endl;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
